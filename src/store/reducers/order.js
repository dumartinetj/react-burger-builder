import * as actions from "../actions/order";

import { updateObject } from "../../utils/utils";

const initialState = {
  orders: [],
  isOrderComplete: false,
  isLoading: false,
  error: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ORDER_INIT:
      return updateObject(state, { isOrderComplete: false });

    case actions.ORDER_BURGER_START:
      return updateObject(state, { isLoading: true, error: false });

    case actions.ORDER_BURGER_SUCCESS:
      return updateObject(state, {
        orders: state.orders.concat(action.orderData),
        isOrderComplete: true,
        isLoading: false
      });

    case actions.ORDER_BURGER_FAILURE:
      return updateObject(state, { isLoading: false, error: action.error });

    case actions.FETCH_ORDERS_START:
      return updateObject(state, { isLoading: true, error: false });

    case actions.FETCH_ORDERS_SUCCESS:
      return updateObject(state, { orders: action.orders, isLoading: false });

    case actions.FETCH_ORDERS_FAILURE:
      return updateObject(state, { isLoading: false, error: action.error });

    default:
      return state;
  }
};

export default reducer;
