import * as actions from "../actions/auth";

import { updateObject } from "../../utils/utils";

const initialState = {
  token: null,
  userId: null,
  isLoading: false,
  redirection: "/",
  error: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.AUTH_START:
      return updateObject(state, { isLoading: true, error: false });

    case actions.AUTH_SUCCESS:
      return updateObject(state, {
        token: action.token,
        userId: action.userId,
        error: false,
        isLoading: false
      });

    case actions.AUTH_FAILURE:
      return updateObject(state, { isLoading: false, error: action.error });

    case actions.AUTH_LOGOUT:
      return updateObject(state, { token: null, userId: null });

    case actions.AUTH_SET_REDIRECTION:
      return updateObject(state, { redirection: action.redirection });

    default:
      return state;
  }
};

export default reducer;
