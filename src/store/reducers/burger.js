import * as actions from "../actions/burger";

import { updateObject } from "../../utils/utils";

const INGREDIENT_PRICES = {
  salad: 0.5,
  bacon: 1,
  cheese: 1,
  meat: 2
};

const initialState = {
  ingredients: null,
  price: 4,
  error: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.ADD_BURGER_INGREDIENT:
      return updateObject(state, {
        ingredients: updateObject(state.ingredients, {
          [action.ingredientName]: state.ingredients[action.ingredientName] + 1
        }),
        price: state.price + INGREDIENT_PRICES[action.ingredientName]
      });

    case actions.REMOVE_BURGER_INGREDIENT:
      return updateObject(state, {
        ingredients: updateObject(state.ingredients, {
          [action.ingredientName]: state.ingredients[action.ingredientName] - 1
        }),
        price: state.price - INGREDIENT_PRICES[action.ingredientName]
      });

    case actions.FETCH_INGREDIENTS_SUCCESS:
      return updateObject(state, {
        ingredients: action.ingredients,
        price: 4,
        error: false
      });

    case actions.FETCH_INGREDIENTS_FAILURE:
      return updateObject(state, {
        error: true
      });

    default:
      return state;
  }
};

export default reducer;
