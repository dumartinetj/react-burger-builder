import reducer from "./auth";
import { AUTH_SUCCESS } from "../actions/auth";

describe("[Redux] Auth Reducer", () => {
  it("should return initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      token: null,
      userId: null,
      isLoading: false,
      redirection: "/",
      error: false
    });
  });

  it("should store token and userId on login", () => {
    expect(
      reducer(undefined, {
        type: AUTH_SUCCESS,
        token: "SomeToken",
        userId: "SomeUserId"
      })
    ).toEqual({
      token: "SomeToken",
      userId: "SomeUserId",
      isLoading: false,
      redirection: "/",
      error: false
    });
  });
});
