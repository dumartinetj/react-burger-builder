import axios from "../../axios-orders";

export const ORDER_INIT = "ORDER_INIT";
export const ORDER_BURGER_START = "ORDER_BURGER_START";
export const ORDER_BURGER_SUCCESS = "ORDER_BURGER_SUCCESS";
export const ORDER_BURGER_FAILURE = "ORDER_BURGER_FAILURE";
export const FETCH_ORDERS_START = "FETCH_ORDERS_START";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_FAILURE = "FETCH_ORDERS_FAILURE";

export const orderInit = () => {
  return { type: ORDER_INIT };
};

export const orderBurgerStart = () => {
  return { type: ORDER_BURGER_START };
};

export const orderBurgerSuccess = (orderData) => {
  return {
    type: ORDER_BURGER_SUCCESS,
    orderData
  };
};

export const orderBurgerFailure = (error) => {
  return { type: ORDER_BURGER_FAILURE, error };
};

export const orderBurger = (orderData, token) => {
  return async (dispatch) => {
    try {
      dispatch(orderBurgerStart());
      const response = await axios.post(
        "/orders.json?auth=" + token,
        orderData
      );
      orderData.id = response.data.name;
      dispatch(orderBurgerSuccess(orderData));
    } catch (e) {
      dispatch(orderBurgerFailure(e));
    }
  };
};

export const fetchOrdersStart = () => {
  return { type: FETCH_ORDERS_START };
};

export const fetchOrdersSuccess = (orders) => {
  return {
    type: FETCH_ORDERS_SUCCESS,
    orders
  };
};

export const fetchOrdersFailure = (error) => {
  return { type: FETCH_ORDERS_FAILURE, error };
};

export const fetchOrders = (token, userId) => {
  return async (dispatch) => {
    try {
      dispatch(fetchOrdersStart());
      const queryParams = `?auth=${token}&orderBy="userId"&equalTo="${userId}"`;
      const response = await axios.get("/orders.json" + queryParams);
      const orders = [];
      for (let key in response.data) {
        orders.push({
          id: key,
          ...response.data[key]
        });
      }
      dispatch(fetchOrdersSuccess(orders));
    } catch (e) {
      dispatch(fetchOrdersFailure(e));
    }
  };
};
