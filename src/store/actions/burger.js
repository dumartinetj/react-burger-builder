import axios from "../../axios-orders";

export const ADD_BURGER_INGREDIENT = "ADD_BURGER_INGREDIENT";
export const REMOVE_BURGER_INGREDIENT = "REMOVE_BURGER_INGREDIENT";
export const FETCH_INGREDIENTS_SUCCESS = "FETCH_INGREDIENTS_SUCCESS";
export const FETCH_INGREDIENTS_FAILURE = "FETCH_INGREDIENTS_FAILURE";

export const addBurgerIngredient = (ingredientName) => {
  return { type: ADD_BURGER_INGREDIENT, ingredientName };
};
export const removeBurgerIngredient = (ingredientName) => {
  return { type: REMOVE_BURGER_INGREDIENT, ingredientName };
};

export const fetchIngredientsSuccess = (ingredients) => {
  return {
    type: FETCH_INGREDIENTS_SUCCESS,
    ingredients: {
      salad: ingredients.salad,
      bacon: ingredients.bacon,
      cheese: ingredients.cheese,
      meat: ingredients.meat
    }
  };
};

export const fetchIngredientsFailure = () => {
  return { type: FETCH_INGREDIENTS_FAILURE };
};

export const fetchIngredientsData = () => {
  return async (dispatch) => {
    try {
      const response = await axios.get("/ingredients.json");
      dispatch(fetchIngredientsSuccess(response.data));
    } catch (e) {
      dispatch(fetchIngredientsFailure());
    }
  };
};
