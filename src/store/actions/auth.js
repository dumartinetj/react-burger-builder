import axios from "axios";

export const AUTH_START = "AUTH_START";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_FAILURE = "AUTH_FAILURE";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const AUTH_SET_REDIRECTION = "AUTH_SET_REDIRECTION";

export const authenticate = (email, password, isSignUp) => {
  return async (dispatch) => {
    dispatch(authStart());
    try {
      const url = isSignUp
        ? `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${process.env.REACT_APP_FIREBASE_KEY}`
        : `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.REACT_APP_FIREBASE_KEY}`;
      const response = await axios.post(url, {
        email,
        password,
        returnSecureToken: true
      });
      localStorage.setItem("token", response.data.idToken);
      localStorage.setItem("userId", response.data.localId);
      const expirationDate = new Date(
        new Date().getTime() + response.data.expiresIn * 1000
      );
      localStorage.setItem("tokenExpiration", expirationDate);
      dispatch(authSuccess(response.data.idToken, response.data.localId));
      dispatch(authCheckTimeout(response.data.expiresIn));
    } catch (e) {
      dispatch(authFailure(e.response.data.error));
    }
  };
};

export const authCheck = () => {
  return (dispatch) => {
    const token = localStorage.getItem("token");
    const userId = localStorage.getItem("userId");
    const tokenExpiration = localStorage.getItem("tokenExpiration");
    const expirationDate = new Date(tokenExpiration);
    if (!token || !tokenExpiration || new Date() > expirationDate) {
      dispatch(authLogout());
    } else {
      dispatch(authSuccess(token, userId));
      dispatch(
        authCheckTimeout(
          (expirationDate.getTime() - new Date().getTime()) / 1000
        )
      );
    }
  };
};

export const authCheckTimeout = (expiration) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(authLogout());
    }, expiration * 1000);
  };
};

export const authStart = () => {
  return { type: AUTH_START };
};

export const authSuccess = (token, userId) => {
  return {
    type: AUTH_SUCCESS,
    token,
    userId
  };
};

export const authFailure = (error) => {
  return {
    type: AUTH_FAILURE,
    error
  };
};

export const authLogout = () => {
  localStorage.removeItem("token");
  localStorage.removeItem("tokenExpiration");
  return { type: AUTH_LOGOUT };
};

export const authSetRedirection = (redirection) => {
  return { type: AUTH_SET_REDIRECTION, redirection };
};
