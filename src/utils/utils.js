export const updateObject = (oldObject, updatedProps) => {
  return {
    ...oldObject,
    ...updatedProps
  };
};

export const fieldValidation = (value, rules) => {
  let isValid = true;

  if (
    rules.required &&
    (value.trim() === "" || value === null || value === undefined)
  ) {
    isValid = false;
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  if (rules.minLength && value.length < rules.minLength) {
    isValid = false;
  }

  if (rules.maxLength && value.length > rules.maxLength) {
    isValid = false;
  }

  return isValid;
};
