import React from "react";
import PropTypes from "prop-types";

import classes from "./Order.module.scss";

function Order({ order }) {
  const ingredients = [];

  for (let ingredientName in order.ingredients) {
    ingredients.push({
      name: ingredientName,
      quantity: order.ingredients[ingredientName]
    });
  }

  const ingredientList = ingredients.map((ingredient) => (
    <li key={ingredient.name}>
      {ingredient.name} ({ingredient.quantity})
    </li>
  ));

  return (
    <div className={classes.Order}>
      <p>
        <b>Ingredients :</b>
      </p>
      <ul>{ingredientList}</ul>
      <p>
        <b>{Number.parseFloat(order.price).toFixed(2)} €</b>
      </p>
    </div>
  );
}

Order.propTypes = {
  order: PropTypes.object.isRequired
};

export default Order;
