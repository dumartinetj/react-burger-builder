import React from "react";
import PropTypes from "prop-types";

import Burger from "../../Burger/Burger";
import Button from "../../UI/Button/Button";

import classes from "./CheckoutSummary.module.scss";

const CheckoutSummary = ({
  ingredients,
  handleCheckoutCancellation,
  handleCheckoutProceed,
}) => {
  return (
    <div className={classes.CheckoutSummary}>
      <h1>Checkout Summary</h1>
      <div className={classes.BurgerContainer}>
        <Burger ingredients={ingredients} />
      </div>
      <Button variant="Danger" clickHandler={handleCheckoutCancellation}>
        CANCEL
      </Button>
      <Button variant="Success" clickHandler={handleCheckoutProceed}>
        CONTINUE
      </Button>
    </div>
  );
};

CheckoutSummary.propTypes = {
  ingredients: PropTypes.object.isRequired,
  handleCheckoutProceed: PropTypes.func.isRequired,
  handleCheckoutCancellation: PropTypes.func.isRequired,
};

export default CheckoutSummary;
