import React from "react";
import PropTypes from "prop-types";

import classes from "./Field.module.scss";

const Field = ({
  fieldtype,
  fieldconfig,
  value,
  validation,
  isValid,
  isTouched,
  changeHandler,
  label
}) => {
  let fieldElement = null;

  const fieldClass = isTouched && !isValid ? classes.Invalid : null;

  switch (fieldtype) {
    case "input":
      fieldElement = (
        <input
          className={fieldClass}
          {...fieldconfig}
          value={value}
          onChange={changeHandler}
        />
      );
      break;

    case "textarea":
      fieldElement = (
        <textarea
          className={fieldClass}
          {...fieldconfig}
          value={value}
          onChange={changeHandler}
        />
      );
      break;

    case "select":
      fieldElement = (
        <select className={fieldClass} value={value} onChange={changeHandler}>
          {fieldconfig.options.map(({ value, label }) => (
            <option key={value} value={value}>
              {label}
            </option>
          ))}
        </select>
      );
      break;

    default:
      fieldElement = (
        <input {...fieldconfig} value={value} onChange={changeHandler} />
      );
  }

  return (
    <div className={classes.Field}>
      {label ? <label>{label}</label> : null}
      {fieldElement}
    </div>
  );
};

Field.propTypes = {
  fieldtype: PropTypes.string.isRequired,
  fieldconfig: PropTypes.object,
  changeHandler: PropTypes.func
};

export default Field;
