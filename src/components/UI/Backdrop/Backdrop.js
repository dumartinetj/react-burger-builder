import React from "react";
import PropTypes from "prop-types";

import classes from "./Backdrop.module.css";

const Backdrop = ({ show, clickHandler }) =>
  show ? <div className={classes.Backdrop} onClick={clickHandler}></div> : null;

Backdrop.propTypes = {
  show: PropTypes.bool.isRequired,
  clickHandler: PropTypes.func.isRequired
};

export default Backdrop;
