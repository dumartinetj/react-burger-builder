import React from "react";
import PropTypes from "prop-types";

import classes from "./Button.module.css";

const Button = ({ variant, clickHandler, children, disabled }) => {
  return (
    <button
      className={[classes.Button, classes[variant]].join(" ")}
      onClick={clickHandler}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  variant: PropTypes.string.isRequired,
  clickHandler: PropTypes.func,
  disabled: PropTypes.bool
};

export default Button;
