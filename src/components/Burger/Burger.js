import React from "react";
import PropTypes from "prop-types";

import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

import classes from "./Burger.module.scss";

const Burger = ({ ingredients }) => {
  // Transform ingredient object to an array of X * BurgerIngredient for each IngredientType
  let ingredientList = Object.keys(ingredients)
    .map(ingredientName => {
      return [...Array(ingredients[ingredientName])].map((_, index) => {
        return (
          <BurgerIngredient
            key={ingredientName + index}
            type={ingredientName}
          />
        );
      });
    })
    .reduce(
      (accumulator, currentValue) => accumulator.concat(currentValue),
      []
    );

  if (ingredientList.length === 0) {
    ingredientList = <p>Add Ingredients</p>;
  }

  return (
    <div className={classes.Burger}>
      <BurgerIngredient type="bread-top" />
      {ingredientList}
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
};

Burger.propTypes = {
  ingredients: PropTypes.object.isRequired
};

export default Burger;
