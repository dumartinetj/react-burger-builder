import React from "react";
import PropTypes from "prop-types";

import BuildControl from "./BuildControl/BuildControl";

import classes from "./BuildControls.module.scss";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" }
];

const BuildControls = ({
  price,
  isValidOrder,
  isAuthenticated,
  disabledControls,
  handleIngredientAddition,
  handleIngredientRemoval,
  handleCheckout
}) => {
  return (
    <div className={classes.BuildControls}>
      <p>
        Current Price : <b>{price.toFixed(2)} €</b>
      </p>
      {controls.map((control) => (
        <BuildControl
          key={control.type}
          label={control.label}
          type={control.type}
          disabled={disabledControls[control.type]}
          handleIngredientAddition={() =>
            handleIngredientAddition(control.type)
          }
          handleIngredientRemoval={() => handleIngredientRemoval(control.type)}
        />
      ))}
      <button
        disabled={!isValidOrder}
        onClick={handleCheckout}
        className={classes.OrderButton}
      >
        {isAuthenticated ? "Buy this delicious burger" : "Sign Up to order"}
      </button>
    </div>
  );
};

// BuildControls.propTypes = {
//   price: PropTypes.number.isRequired,
//   isValidOrder: PropTypes.bool.isRequired,
//   isAuthenticated: PropTypes.bool.isRequired,
//   disabledControls: PropTypes.object.isRequired,
//   handleIngredientAddition: PropTypes.func.isRequired,
//   handleIngredientRemoval: PropTypes.func.isRequired,
//   handleCheckout: PropTypes.func.isRequired
// };

export default BuildControls;
