import React from "react";
import PropTypes from "prop-types";

import classes from "./BuildControl.module.scss";

const BuildControl = ({
  label,
  disabled,
  handleIngredientAddition,
  handleIngredientRemoval
}) => {
  return (
    <div className={classes.BuildControl}>
      <div className={classes.Label}>{label}</div>
      <button
        onClick={handleIngredientRemoval}
        disabled={disabled}
        className={classes.Less}
      >
        Less
      </button>
      <button onClick={handleIngredientAddition} className={classes.More}>
        More
      </button>
    </div>
  );
};

BuildControl.propTypes = {
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  handleIngredientAddition: PropTypes.func.isRequired,
  handleIngredientRemoval: PropTypes.func.isRequired
};

export default BuildControl;
