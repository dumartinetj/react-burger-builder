import React from "react";
import PropTypes from "prop-types";

import Button from "../../UI/Button/Button";

const BurgerRecap = ({
  ingredients,
  price,
  handleCheckoutCancellation,
  handleCheckoutStart
}) => {
  const ingredientList = Object.keys(ingredients).map((ingredientName) => {
    return (
      <li key={ingredientName}>
        <span style={{ textTransform: "capitalize" }}>{ingredientName}</span> :{" "}
        {ingredients[ingredientName]}
      </li>
    );
  });
  return (
    <>
      <h3>Your Custom' Burger</h3>
      <p>Your delicious burger is composed of the following ingredients :</p>
      <ul>
        {ingredientList}
        <li>
          <b>Total price : {price.toFixed(2)} €</b>
        </li>
      </ul>
      <p>One more step to eat this bad boy !</p>
      <Button variant="Danger" clickHandler={handleCheckoutCancellation}>
        CANCEL
      </Button>
      <Button variant="Success" clickHandler={handleCheckoutStart}>
        CHECKOUT
      </Button>
    </>
  );
};

// BurgerRecap.propTypes = {
//   ingredients: PropTypes.object.isRequired,
//   price: PropTypes.number.isRequired,
//   handleCheckoutCancellation: PropTypes.func.isRequired,
//   handleCheckoutStart: PropTypes.func.isRequired
// };

export default BurgerRecap;
