import React from "react";
import PropTypes from "prop-types";

import Backdrop from "../../UI/Backdrop/Backdrop";
import Logo from "../Logo/Logo";
import Navigation from "../Navigation/Navigation";

import classes from "./Sidebar.module.scss";

const Sidebar = ({ isSidebarOpen, isAuthenticated, handleSidebarClosing }) => {
  const sidebarClasses = [
    classes.Sidebar,
    isSidebarOpen ? classes.Open : classes.Close
  ];
  return (
    <>
      <Backdrop show={isSidebarOpen} clickHandler={handleSidebarClosing} />
      <div className={sidebarClasses.join(" ")} onClick={handleSidebarClosing}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <nav>
          <Navigation isAuthenticated={isAuthenticated} />
        </nav>
      </div>
    </>
  );
};

Sidebar.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  handleSidebarClosing: PropTypes.func.isRequired
};

export default Sidebar;
