import React from "react";
import PropTypes from "prop-types";

import classes from "./SidebarToggler.module.scss";

const SidebarToggler = ({ handleSidebarToggle }) => {
  return (
    <div className={classes.SidebarToggler} onClick={handleSidebarToggle}>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

SidebarToggler.propTypes = {
  handleSidebarToggle: PropTypes.func.isRequired
};

export default SidebarToggler;
