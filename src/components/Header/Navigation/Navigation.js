import React from "react";
import PropTypes from "prop-types";

import NavigationItem from "./NavigationItem/NavigationItem";

import classes from "./Navigation.module.scss";

const Navigation = ({ isAuthenticated }) => {
  return (
    <ul className={classes.Navigation}>
      <NavigationItem link="/" exact>
        Build my burger !
      </NavigationItem>
      {isAuthenticated ? (
        <>
          <NavigationItem link="/orders">Orders</NavigationItem>
          <NavigationItem link="/logout">Logout</NavigationItem>
        </>
      ) : (
        <NavigationItem link="/auth">Login</NavigationItem>
      )}
    </ul>
  );
};

Navigation.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
};

export default Navigation;
