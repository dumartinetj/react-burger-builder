import React, { Component } from "react";

import Logo from "./Logo/Logo";
import Navigation from "./Navigation/Navigation";
import SidebarToggler from "./Sidebar/SidebarToggler/SidebarToggler";
import Sidebar from "./Sidebar/Sidebar";

import classes from "./Header.module.scss";

class Header extends Component {
  state = {
    isSidebarOpen: false
  };

  handleSidebarToggle = () => {
    this.setState((prevState) => {
      return {
        isSidebarOpen: !prevState.isSidebarOpen
      };
    });
  };

  handleSidebarClosing = () => {
    this.setState({
      isSidebarOpen: false
    });
  };

  render() {
    return (
      <header className={classes.Header}>
        <div className={classes.Logo}>
          <Logo />
        </div>

        <nav className={classes.DesktopOnly}>
          <Navigation isAuthenticated={this.props.isAuthenticated} />
        </nav>

        <SidebarToggler handleSidebarToggle={this.handleSidebarToggle} />

        <Sidebar
          isSidebarOpen={this.state.isSidebarOpen}
          isAuthenticated={this.props.isAuthenticated}
          handleSidebarClosing={this.handleSidebarClosing}
        />
      </header>
    );
  }
}

export default Header;
