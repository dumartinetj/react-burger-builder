import React from "react";

import classes from "./Logo.module.scss";
import burgerLogo from "../../../assets/images/logo.png";

const Logo = () => (
  <div className={classes.Logo}>
    <img src={burgerLogo} alt="Custom' Burger " />
  </div>
);

export default Logo;
