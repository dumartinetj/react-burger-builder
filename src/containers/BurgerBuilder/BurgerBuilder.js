import React, { Component } from "react";
import { connect } from "react-redux";

import axios from "../../axios-orders";
import {
  fetchIngredientsData,
  addBurgerIngredient,
  removeBurgerIngredient,
  orderInit,
  authSetRedirection
} from "../../store/actions";

import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import BurgerRecap from "../../components/Burger/BurgerRecap/BurgerRecap";
import Loader from "../../components/UI/Loader/Loader";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

export class BurgerBuilder extends Component {
  state = {
    checkout: false
  };

  componentDidMount() {
    this.props.fetchIngredientsData();
  }

  isValidOrder = () => {
    const ingredientCount = Object.keys(this.props.ingredients)
      .map((ingredientName) => this.props.ingredients[ingredientName])
      .reduce((ingredientCount, element) => ingredientCount + element, 0);

    return ingredientCount > 0;
  };

  handleCheckout = () => {
    if (this.props.isAuthenticated) {
      this.setState({ checkout: true });
    } else {
      this.props.handleSetRedirection("/checkout");
      this.props.history.push("/auth");
    }
  };

  handleCheckoutStart = () => {
    this.props.handleOrderInit();
    this.props.history.push("/checkout");
  };

  handleCheckoutCancellation = () => {
    this.setState({ checkout: false });
  };

  render() {
    const disabledControls = {
      ...this.props.ingredients
    };
    for (let key in disabledControls) {
      disabledControls[key] = disabledControls[key] <= 0;
    }

    let burgerRecap = null;
    let burgerBuilder = this.props.error ? (
      <p>Ingredients can't be loaded</p>
    ) : (
      <Loader />
    );

    if (this.props.ingredients) {
      burgerBuilder = (
        <>
          <Burger ingredients={this.props.ingredients} />
          <BuildControls
            price={this.props.price}
            isValidOrder={this.isValidOrder()}
            isAuthenticated={this.props.isAuthenticated}
            disabledControls={disabledControls}
            handleIngredientAddition={this.props.handleIngredientAddition}
            handleIngredientRemoval={this.props.handleIngredientRemoval}
            handleCheckout={this.handleCheckout}
          />
        </>
      );

      burgerRecap = (
        <BurgerRecap
          ingredients={this.props.ingredients}
          price={this.props.price}
          handleCheckoutCancellation={this.handleCheckoutCancellation}
          handleCheckoutStart={this.handleCheckoutStart}
        />
      );
    }
    return (
      <>
        {burgerBuilder}
        <Modal
          show={this.state.checkout}
          closingHandler={this.handleCheckoutCancellation}
        >
          {burgerRecap}
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ingredients: state.burger.ingredients,
    price: state.burger.price,
    error: state.burger.error,
    isAuthenticated: state.auth.token !== null
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchIngredientsData: () => dispatch(fetchIngredientsData()),
    handleIngredientAddition: (ingredientName) =>
      dispatch(addBurgerIngredient(ingredientName)),
    handleIngredientRemoval: (ingredientName) =>
      dispatch(removeBurgerIngredient(ingredientName)),
    handleOrderInit: () => dispatch(orderInit()),
    handleSetRedirection: (redirection) =>
      dispatch(authSetRedirection(redirection))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(BurgerBuilder, axios));
