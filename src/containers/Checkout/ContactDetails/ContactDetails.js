import React, { Component } from "react";
import { connect } from "react-redux";

import axios from "../../../axios-orders";
import { orderBurger } from "../../../store/actions";
import { updateObject, fieldValidation } from "../../../utils/utils";

import Button from "../../../components/UI/Button/Button";
import Field from "../../../components/UI/Field/Field";
import Loader from "../../../components/UI/Loader/Loader";
import withErrorHandler from "../../../hoc/withErrorHandler/withErrorHandler";

import classes from "./ContactDetails.module.scss";

class ContactDetails extends Component {
  state = {
    form: {
      name: {
        fieldtype: "input",
        fieldconfig: {
          type: "text",
          placeholder: "Your Name"
        },
        value: "",
        validation: {
          required: true
        },
        isValid: false,
        isTouched: false
      },
      email: {
        fieldtype: "input",
        fieldconfig: {
          type: "email",
          placeholder: "Your E-mail"
        },
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        isValid: false,
        isTouched: false
      },
      street: {
        fieldtype: "input",
        fieldconfig: {
          type: "text",
          placeholder: "Your Address"
        },
        value: "",
        validation: {
          required: true
        },
        isValid: false,
        isTouched: false
      },
      zip: {
        fieldtype: "input",
        fieldconfig: {
          type: "text",
          placeholder: "Your Zip Code"
        },
        value: "",
        validation: {
          required: true,
          minLength: 3,
          maxLength: 5
        },
        isValid: false,
        isTouched: false
      },
      city: {
        fieldtype: "input",
        fieldconfig: {
          type: "text",
          placeholder: "Your City"
        },
        value: "",
        validation: {
          required: true
        },
        isValid: false,
        isTouched: false
      },
      deliveryMethod: {
        fieldtype: "select",
        fieldconfig: {
          options: [
            { value: "fastest", label: "Fastest" },
            { value: "cheapest", label: "Cheapest" }
          ]
        },
        value: "fastest",
        validation: {},
        isValid: true,
        isTouched: false
      }
    },
    formIsValid: false
  };

  handleFieldChange = (event, fieldname) => {
    const updatedForm = updateObject(this.state.form, {
      [fieldname]: updateObject(this.state.form[fieldname], {
        value: event.target.value,
        isValid: fieldValidation(
          event.target.value,
          this.state.form[fieldname].validation
        ),
        isTouched: true
      })
    });

    let formIsValid = true;
    for (let fieldname in updatedForm) {
      formIsValid = updatedForm[fieldname].isValid && formIsValid;
    }

    this.setState({
      form: updatedForm,
      formIsValid
    });
  };

  handleFormSubmission = async (event) => {
    event.preventDefault();

    const orderData = {};
    for (let fieldname in this.state.form) {
      orderData[fieldname] = this.state.form[fieldname].value;
    }

    orderData.ingredients = this.props.ingredients;
    orderData.price = this.props.price;
    orderData.userId = this.props.userId;

    this.props.handleBurgerOrder(orderData, this.props.token);
  };

  render() {
    const formFields = [];
    for (let fieldname in this.state.form) {
      let field = this.state.form[fieldname];
      formFields.push({
        id: fieldname,
        ...field
      });
    }

    let form = (
      <form onSubmit={this.handleFormSubmission}>
        {formFields.map((formField) => (
          <Field
            key={formField.id}
            {...formField}
            changeHandler={(event) =>
              this.handleFieldChange(event, formField.id)
            }
          />
        ))}

        <Button
          type="submit"
          disabled={!this.state.formIsValid}
          variant="Success"
        >
          ORDER
        </Button>
      </form>
    );
    if (this.props.isLoading) {
      form = <Loader />;
    }

    return (
      <div className={classes.ContactDetails}>
        <h4>Contact Details</h4>
        {form}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ingredients: state.burger.ingredients,
    price: state.burger.price,
    isLoading: state.order.isLoading,
    token: state.auth.token,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleBurgerOrder: (orderData, token) =>
      dispatch(orderBurger(orderData, token))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ContactDetails, axios));
