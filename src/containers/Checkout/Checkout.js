import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import CheckoutSummary from "../../components/Checkout/CheckoutSummary/CheckoutSummary";
import ContactDetails from "./ContactDetails/ContactDetails";

class Checkout extends Component {
  handleCheckoutProceed = () => {
    this.props.history.replace("/checkout/contact");
  };

  handleCheckoutCancellation = () => {
    this.props.history.goBack();
  };

  render() {
    let summary = <Redirect to="/" />;
    if (this.props.ingredients && !this.props.isOrderComplete) {
      summary = (
        <div>
          <CheckoutSummary
            ingredients={this.props.ingredients}
            handleCheckoutProceed={this.handleCheckoutProceed}
            handleCheckoutCancellation={this.handleCheckoutCancellation}
          />
          <Route
            path={this.props.match.path + "/contact"}
            component={ContactDetails}
          />
        </div>
      );
    }
    return summary;
  }
}

const mapStateToProps = (state) => {
  return {
    ingredients: state.burger.ingredients,
    isOrderComplete: state.order.isOrderComplete
  };
};

export default connect(mapStateToProps)(Checkout);
