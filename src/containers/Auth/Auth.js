import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { authenticate, authSetRedirection } from "../../store/actions";
import { updateObject, fieldValidation } from "../../utils/utils";

import Field from "../../components/UI/Field/Field";
import Button from "../../components/UI/Button/Button";
import Loader from "../../components/UI/Loader/Loader";

import classes from "./Auth.module.scss";

class Auth extends Component {
  state = {
    form: {
      email: {
        fieldtype: "input",
        fieldconfig: {
          type: "email",
          placeholder: "E-mail"
        },
        value: "",
        validation: {
          required: true,
          isEmail: true
        },
        isValid: false,
        isTouched: false
      },
      password: {
        fieldtype: "input",
        fieldconfig: {
          type: "password",
          placeholder: "Password "
        },
        value: "",
        validation: {
          required: true,
          minLength: 6
        },
        isValid: false,
        isTouched: false
      }
    },
    formIsValid: false,
    isSignUp: false
  };

  handleSignupToggle = () => {
    this.setState((prevState) => {
      return {
        isSignUp: !prevState.isSignUp
      };
    });
  };

  handleFieldChange = (event, fieldname) => {
    const updatedForm = updateObject(this.state.form, {
      [fieldname]: updateObject(this.state.form[fieldname], {
        value: event.target.value,
        isValid: fieldValidation(
          event.target.value,
          this.state.form[fieldname].validation
        ),
        isTouched: true
      })
    });

    let formIsValid = true;
    for (let fieldname in updatedForm) {
      formIsValid = updatedForm[fieldname].isValid && formIsValid;
    }

    this.setState({
      form: updatedForm,
      formIsValid
    });
  };

  handleFormSubmission = async (event) => {
    event.preventDefault();

    this.props.handleAuthentication(
      this.state.form.email.value,
      this.state.form.password.value,
      this.state.isSignUp
    );
  };

  componentWillUnmount() {
    if (this.props.redirection !== "/") {
      this.props.handleResetRedirection();
    }
  }

  render() {
    const formFields = [];
    for (let fieldname in this.state.form) {
      let field = this.state.form[fieldname];
      formFields.push({
        id: fieldname,
        ...field
      });
    }

    let form = this.props.isLoading ? (
      <Loader />
    ) : (
      <form onSubmit={this.handleFormSubmission}>
        {formFields.map((formField) => (
          <Field
            key={formField.id}
            {...formField}
            changeHandler={(event) =>
              this.handleFieldChange(event, formField.id)
            }
          />
        ))}

        <Button
          type="submit"
          disabled={!this.state.formIsValid}
          variant="Success"
        >
          {this.state.isSignUp ? "Sign Up" : "Sign In"}
        </Button>
      </form>
    );

    const redirectPath = this.props.redirection;
    let redirect = null;
    if (this.props.isAuthenticated) {
      redirect = <Redirect to={redirectPath} />;
    }

    return (
      <div className={classes.Auth}>
        <h1>{this.state.isSignUp ? "Sign Up" : "Sign In"}</h1>

        {redirect}

        {this.props.error ? (
          <p>
            <b>Erreur :</b> {this.props.error.message}
          </p>
        ) : null}

        {form}
        <Button clickHandler={this.handleSignupToggle} variant="Danger">
          Switch to {this.state.isSignUp ? "Sign In" : "Sign Up"}
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.auth.isLoading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    redirection: state.auth.redirection
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAuthentication: (email, password, isSignUp) =>
      dispatch(authenticate(email, password, isSignUp)),
    handleResetRedirection: () => dispatch(authSetRedirection("/"))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
