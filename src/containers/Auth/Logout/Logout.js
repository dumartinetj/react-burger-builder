import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { authLogout } from "../../../store/actions";

class Logout extends Component {
  componentDidMount() {
    this.props.handleLogout();
  }
  render() {
    return <Redirect to="/" />;
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleLogout: () => dispatch(authLogout())
  };
};

export default connect(null, mapDispatchToProps)(Logout);
