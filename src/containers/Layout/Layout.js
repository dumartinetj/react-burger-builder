import React from "react";

import { connect } from "react-redux";

import Header from "../../components/Header/Header";

import classes from "./Layout.module.scss";

const Layout = ({ isAuthenticated, children }) => (
  <>
    <Header isAuthenticated={isAuthenticated} />
    <main className={classes.Content}>{children}</main>
  </>
);

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.token !== null
  };
};

export default connect(mapStateToProps)(Layout);
