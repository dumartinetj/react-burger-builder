import React, { Component } from "react";
import { connect } from "react-redux";

import * as actions from "../../store/actions";
import axios from "../../axios-orders";

import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import Order from "../../components/Order/Order";
import Loader from "../../components/UI/Loader/Loader";

class Orders extends Component {
  componentDidMount() {
    this.props.handleOrdersFetching(this.props.token, this.props.userId);
  }

  render() {
    let orders = <Loader />;
    if (!this.props.isLoading && this.props.orders) {
      orders = this.props.orders.map((order) => (
        <Order key={order.id} order={order} />
      ));
    }

    return <div>{orders}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    orders: state.order.orders,
    isLoading: state.order.isLoading,
    token: state.auth.token,
    userId: state.auth.userId
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOrdersFetching: (token, userId) =>
      dispatch(actions.fetchOrders(token, userId))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(Orders, axios));
